Algorithmus:
Setze Register 1-200 auf 0
Für alle Zahlen i [2:14]  {
	j=i*i
	solange(j<=200){
		setze Register i auf 1
		j=j+i
	}
}

.def zero=R1
.def i=R2
.def j=R3
.def one=R4

Filler:
	LDI zero, 0
	LDI i, 0
	CLR R27			; Löscht 
	LDI R26, $0100 	; R26 ist das Lower Byte des X-Register S.11
FillerLoop:
	ST X+, zero 	; Schreibt zero in die Speicheradresse die im lower Byte des X Registers steht und inkrementiert die Adresse dann um 1
	INC i			; Inkrementiert R2 um 1
	CPI i, 200 		; Vergleicht R2 mit 200
	BRLO FillerLoop	; Springt zu FillerLoop falls R2 kleiner als 200 war

Sieb:
	LDI i, 2		; Läd 2 in R2
	LDI one, 1		; Läd 1 in R4
SiebOuterLoop:
	LDI R26, $0100	; Wieder die Erste Adresse in das Register Laden um von Vorne anzufangen
	LDI	j, i		; Läd i in j 
SiebInnerLoop:
	ADD R26, i		; Startadresse bei Start+i und bei jedem Schleifenaufruf i Speicheradressen weiter gehen
	ST X, one		; Lade 1 in die Speicheradresse
	ADD j, i		; Zähle j hoch als Abbruchbedingung für die Schleife
EndInnerLoop:		; Abbruch für innere Schleife
	CPI j, 200
	BRLO SiebInnerLoop
EndOuterLoop:		; Abbruch für äußere Schleife
	INC i
	CPI i, 14 		; Vergleicht R2 mit 14
	BRLO SiebOuterLoop	; Springt zu SiebOuterLoop falls R2 kleiner als 14 war